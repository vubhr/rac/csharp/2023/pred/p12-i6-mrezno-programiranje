﻿namespace SocketServerGUI {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbPoruke = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbPoruke
            // 
            this.lbPoruke.FormattingEnabled = true;
            this.lbPoruke.Location = new System.Drawing.Point(12, 12);
            this.lbPoruke.Name = "lbPoruke";
            this.lbPoruke.Size = new System.Drawing.Size(468, 459);
            this.lbPoruke.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 484);
            this.Controls.Add(this.lbPoruke);
            this.Name = "Form1";
            this.Text = "Socket Server GUI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox lbPoruke;
    }
}

